package com.example.Routes

import com.example.model.Comentari
import com.example.model.Pelicula
import com.example.model.listaPeliculas
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.moviesRouting(){
    route("/peliculas"){
        get {
            if(listaPeliculas.isNotEmpty()) call.respond(listaPeliculas)
            else call.respondText("No hay peliculas en la lista", status = HttpStatusCode.OK)
        }
        get("{id?}") {
            if(call.parameters["id"].isNullOrBlank()) return@get call.respondText("No hay id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            for(pelicula in listaPeliculas){
                if(pelicula.id == id) return@get call.respond(pelicula)
            }
            call.respondText("No se ha encontrado la pelicula con id $id", status = HttpStatusCode.NotFound)
        }
        post{
            val pelicula = call.receive<Pelicula>()
            listaPeliculas.add(pelicula)
            call.respondText("Customer stored correctly", status = HttpStatusCode.Created)
        }
        post("{id?}"){
            if(call.parameters["id"].isNullOrBlank()) return@post call.respondText("No hay id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            val comentario = call.receive<Comentari>()
            for(i in listaPeliculas){
                if(i.id == id){
                     for(j in i.comentarios){
                         //if(comentario.idDePelicula == j.idDePelicula)return@post call.respondText("La pelicula con id $id ya tiene un comentario", status = HttpStatusCode.Conflict)
                         if(j.id == comentario.id) return@post call.respondText("El id del comentario ya esta registrado", status = HttpStatusCode.Conflict)
                     }
                    i.comentarios.add(comentario)
                    return@post call.respondText("Comentario añadido con exito", status = HttpStatusCode.Created)
                }
            }
            call.respondText("No se ha encontrado esa id", status = HttpStatusCode.Created)
        }
        put ("{id?}"){
            if(call.parameters["id"].isNullOrBlank()) return@put call.respondText("No hay id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            val newPelicula = call.receive<Pelicula>()

            for(i in listaPeliculas){
                if(i.id == id){
                    i.comentarios = newPelicula.comentarios
                    i.any = newPelicula.any
                    i.genere = newPelicula.genere
                    i.director = newPelicula.director
                    i.titol = newPelicula.titol
                    return@put call.respondText("Actualizado con exito", status = HttpStatusCode.OK)
                }
            }

            call.respondText("No se ha encontrado la pelicula con id $id", status = HttpStatusCode.NotFound)
        }
        delete("{id?}") {
            if(call.parameters["id"].isNullOrBlank()) return@delete call.respondText("No hay id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            for(i in listaPeliculas){
                if(i.id == id){
                    listaPeliculas.remove(i)
                    return@delete call.respondText("Borrado con exito", status = HttpStatusCode.OK)
                }
            }
            call.respondText("No se ha encontrado la pelicula con id $id", status = HttpStatusCode.NotFound)
        }
        get("{id?}/comentarios") {
            if(call.parameters["id"].isNullOrBlank()) return@get call.respondText("No hay id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            for(i in listaPeliculas){
                if(i.id == id){
                    return@get call.respond(i.comentarios)
                }
            }
            call.respondText("No se ha encontrado la pelicula con id $id", status = HttpStatusCode.NotFound)
        }
    }
}