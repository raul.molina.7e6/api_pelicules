package com.example.plugins

import io.ktor.server.routing.*
import io.ktor.server.application.*
import com.example.Routes.moviesRouting

fun Application.configureRouting() {
    routing {
       moviesRouting()
    }
}
