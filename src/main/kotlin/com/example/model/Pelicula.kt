package com.example.model

import kotlinx.serialization.Serializable

@Serializable
data class Pelicula(
    var id: String,
    var titol: String,
    var any: String,
    var genere: String,
    var director: String,
    var comentarios: MutableList<Comentari>
)

val listaPeliculas = mutableListOf<Pelicula>()