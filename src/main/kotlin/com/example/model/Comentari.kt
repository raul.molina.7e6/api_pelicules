package com.example.model

import kotlinx.serialization.Serializable


@Serializable
data class Comentari(
    val id: String,
    val idDePelicula: String,
    var comentari: String,
    val dataDeCreacio: String
)